**DEPRECATED, NO LONGER MAINTAINED [Use this version instead](https://gitlab.com/lionlab-company/golang/lionrouter)**
# LionRouter (Golang path-trie router)
This is a leightweight minimalistic impelementation of a go router based on a path trie approach. Don't expect a fully feautured router now or in future. Only the most necessary features will be included for now and future.

# Supported functionalities

- implements `http.Handler` interface
- routes
    - static
    - parameter routes (key and path)
- subrouting

# How to use

The router implements the `http.Handler` interface, so it can be used alongside with other frameworks. Each route handler is using the `http.Handler` interface itself to maximize compatibility with other packages.

## New router instance
`NewRouter() *Router`
```
router := lionrouter.NewRouter()
```

## Define a route
To define routes use either the generic Handler method or one of the short versions.

`Handler(method string, path string, handler http.Handler)`

`Get(path string, handler http.Handler)`

`Post(path string, handler http.Handler)`

`Put(path string, handler http.Handler)`

`Patch(path string, handler http.Handler)`

`Delete(path string, handler http.Handler)`

`Head(path string, handler http.Handler)`

`Options(path string, handler http.Handler)`

### Static route

```
router.Handler(http.MethodGet, "/contact", contactHandler())
```
or
```
router.Get("/contact", contactHandler())
```

Each request to `http://.../contact` will be passed through the contactHandler() method.

### Parameter route

#### Named key

```
router.Handler(http.MethodGet, "/download/:file", downloadHandler())
```

Each request to `http://.../download/someFileName` will be passed through the downloadHandler() method where you can get the corresponding value passed by as `file` by extracing it from the `context`.

```
params, ok := lionrouter.Params(r.Context())

if ok {
    w.Write([]byte(params["file"]))
}
```
or
```
w.Write([]byte(lionrouter.Param(r.Context(), "file"))
```

This will generate the output: `someFileName`.
A request like `http://.../download/profile_picture.png` would get: `profile_picture.png`.

Multiple named keys per route are possible:

```
router.Handler(http.MethodGet, "/download/:param1/:param2", downloadHandler())
```

#### Named path

Named path is corresponding to the named key functionality except a whole path is read from request.

```
router.Handler(http.MethodGet, "/download/:file*", downloadHandler())
```

Each request to `http://.../download/some/path/here/test.png` will be passed through the downloadHandler() method where you can get the corresponding value passed by as `file` by extracing it from the `context`.

```
params, ok := lionrouter.Params(r.GetContext())

if ok {
    w.Write([]byte(params["file"]))
}
```

This will generate the output: `/some/path/here/test.png`.

A named path can also be combined with named keys:

```
router.Handler(http.MethodGet, "/download/:param1/:param2/:param3*", downloadHandler())
```
**Named paths are only possible at the end.**

## Sub-Routing

You can register any http.Handler to a given path. The request will then be passed to the registered Handler. Note that the http.StripPrefix() middleware will be chained before passing through the request to the sub-router handler.

`Router(path string, handler http.Handler)`

```
mainRouter := lionrouter.NewRouter()
staticRouter := lionrouter.NewRouter()

staticRouter.Get("/:file*", staticHandler())
mainRouter.Router("/static", staticRouter)
```

**Named paths will break sub-routing, if used alongside in the same path.**

## Middleware

To assign any given middleware of the type `func(http.Handler) http.Handler`, just use the `Middleware(func(http.Handler) Handler)` method.

```
router.Middleware(someMiddleware)
```

# License

MIT licensed 2017-2018 Cedrik Kaufmann. See the LICENSE file for further details.