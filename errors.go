// Copyright 2017-2018 Cedrik Kaufmann. All rights reserved.
// Use of this source code is governed by a MIT-License
// license that can be found in the LICENSE file.

// Default routing errors

package lionrouter

import "errors"

// Routing errors
var (
	ErrHandler         = errors.New("No nil handler allowed")
	ErrMiddleware      = errors.New("No nil middleware allowed")
	ErrInvalidPath     = errors.New("Path has to start with '/'")
	ErrPathAssignment  = errors.New("Unable to assign path")
	ErrInternalRouting = errors.New("Internal routing error")
)
