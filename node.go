// Copyright 2017-2018 Cedrik Kaufmann. All rights reserved.
// Use of this source code is governed by a MIT-License
// license that can be found in the LICENSE file.

// Simple trie node implementation

package lionrouter

import (
	"net/http"
)

// node represents a node in a path trie
type node struct {
	isNamedKey  bool
	isNamedPath bool
	isRouter    bool
	isWildChild bool
	key         string
	children    map[string]*node
	handler     map[string]http.Handler
}

// newNode creates a new trie node with default settings
// returns trie node pointer
func newNode() *node {
	return &node{
		isNamedKey:  false,
		isNamedPath: false,
		isWildChild: false,
		isRouter:    false,
		key:         "",
		children:    make(map[string]*node),
		handler:     nil,
	}
}

// setNodeProperties sets the node properties to given setting object
func (node *node) setNodeProperties(properties keyProperties) {
	node.isNamedKey = properties.isNamedKey
	node.isNamedPath = properties.isNamedPath
	node.key = properties.key
}
