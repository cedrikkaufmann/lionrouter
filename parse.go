package lionrouter

import (
	"strings"
)

// keyProperties representing settings of a trie node
type keyProperties struct {
	isNamedKey  bool
	isNamedPath bool
	key         string
}

// parsePath parses an path address
// It returns the single subpaths as slice
func parsePath(path string) ([]string, error) {
	// check if path starts with '/'
	if path[0] != '/' {
		return nil, ErrInvalidPath
	}

	// split path by '/'
	keys := strings.Split(path, "/")

	// remove first element which should be an empty string
	keys = keys[1:]

	// remove last element if path ends with '/'
	if keys[len(keys)-1] == "" {
		keys = keys[:len(keys)-1]
	}

	return keys, nil
}

// parseKey parses an given path key
// It returns the properties of the given key
func parseKey(key string) keyProperties {
	// create default properties
	properties := keyProperties{
		isNamedKey:  false,
		isNamedPath: false,
	}

	keyLength := len(key)

	// check whether key is named key or named path
	if keyLength >= 2 && key[0] == ':' {
		if keyLength >= 3 && key[keyLength-1] == '*' {
			// named path key
			properties.isNamedPath = true
			properties.key = key[1 : keyLength-1]
		} else {
			// named key
			properties.isNamedKey = true
			properties.key = key[1:]
		}
	} else {
		// assign key name
		properties.key = key
	}

	return properties
}
