// Copyright 2017-2018 Cedrik Kaufmann. All rights reserved.
// Use of this source code is governed by a MIT-License
// license that can be found in the LICENSE file.

// Path trie based http router

package lionrouter

import (
	"context"
	"log"
	"net/http"
)

// ContextKey represents the request context entries
type ContextKey int

const (
	contextParams ContextKey = 0 << iota
)

// Router defines a path trie based Router
type Router struct {
	pathTrie     *pathTrie
	middleware   func(http.Handler) http.Handler
	errorHandler map[int]http.Handler
}

// NewRouter creates a new path-trie based router
func NewRouter() *Router {
	return &Router{
		pathTrie:     newPathTrie(),
		errorHandler: make(map[int]http.Handler),
	}
}

// Handler registers a http.Handler
func (router *Router) Handler(method string, path string, handler http.Handler) {
	err := router.pathTrie.add(method, path, handler)

	if err != nil {
		panic(err)
	}
}

// Get registers a http.Handler as HTTP-GET Method
func (router *Router) Get(path string, handler http.Handler) {
	router.Handler(http.MethodGet, path, handler)
}

// Post registers a http.Handler as HTTP-POST Method
func (router *Router) Post(path string, handler http.Handler) {
	router.Handler(http.MethodPost, path, handler)
}

// Put registers a http.Handler as HTTP-PUT Method
func (router *Router) Put(path string, handler http.Handler) {
	router.Handler(http.MethodPut, path, handler)
}

// Patch registers a http.Handler as HTTP-PATCH Method
func (router *Router) Patch(path string, handler http.Handler) {
	router.Handler(http.MethodPatch, path, handler)
}

// Delete registers a http.Handler as HTTP-DELETE Method
func (router *Router) Delete(path string, handler http.Handler) {
	router.Handler(http.MethodDelete, path, handler)
}

// Head registers a http.Handler as HTTP-HEAD Method
func (router *Router) Head(path string, handler http.Handler) {
	router.Handler(http.MethodHead, path, handler)
}

// Options registers a http.Handler as HTTP-OPTIONS Method
func (router *Router) Options(path string, handler http.Handler) {
	router.Handler(http.MethodOptions, path, handler)
}

// ErrorHandler registers an http.Handler for error handling
func (router *Router) ErrorHandler(errorCode int, handler http.Handler) {
	router.errorHandler[errorCode] = handler
}

// Router registers a http.Handler as sub-router
func (router *Router) Router(path string, handler http.Handler) {
	err := router.pathTrie.addRouter(path, http.StripPrefix(path, handler))

	if err != nil {
		panic(err)
	}
}

// Middleware registers a middleware before as global router middleware
func (router *Router) Middleware(adapter func(http.Handler) http.Handler) {
	if adapter != nil {
		router.middleware = adapter
	} else {
		panic(ErrMiddleware)
	}
}

// ServeHTTP handles a given request
func (router *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get path of current request
	path := r.URL.Path

	// get current context
	ctx := r.Context()

	// get handler from trie
	handler, params, err := router.pathTrie.get(r.Method, path)

	// pass request through 500 handler if error occurred
	if err != nil {
		defer log.Fatal(err)

		if handler := router.errorHandler[http.StatusInternalServerError]; handler != nil {
			handler.ServeHTTP(w, r.WithContext(ctx))
		}
	}

	// check if handler exists
	if handler != nil {
		// check if params are given and add them to the current context
		if params != nil {
			ctx = context.WithValue(ctx, contextParams, params)
		}

		// pass through the request to the given http.Handler
		if router.middleware != nil {
			router.middleware(handler).ServeHTTP(w, r.WithContext(ctx))
		} else {
			handler.ServeHTTP(w, r.WithContext(ctx))
		}
	} else if handler := router.errorHandler[http.StatusNotFound]; handler != nil {
		// pass request through 404 handler if no handler for path
		handler.ServeHTTP(w, r.WithContext(ctx))
	}
}

// Params extracts the params map from the given context
// It returns the params string map or nil
func Params(ctx context.Context) (map[string]string, bool) {
	// get params from context
	params, ok := ctx.Value(contextParams).(map[string]string)

	// check if params exist in given context and return them
	if ok {
		return params, true
	}

	// return error because no params exist
	return nil, false
}

// Param extracts the single param value from the given context and key
// It returns the param as string or an empty string
func Param(ctx context.Context, key string) string {
	// get params from context
	params, ok := ctx.Value(contextParams).(map[string]string)

	// check if params exist in given context and return them
	if ok {
		return params[key]
	}

	// return empty string because no param exists
	return ""
}
