// Copyright 2017-2018 Cedrik Kaufmann. All rights reserved.
// Use of this source code is governed by a MIT-License
// license that can be found in the LICENSE file.

// Path trie implementation

package lionrouter

import (
	"log"
	"net/http"

	"github.com/oxtoacart/bpool"
)

// pathTrie represents trie root
type pathTrie struct {
	root  *node
	bpool *bpool.BufferPool
}

// newPathTrie creates a new trie
// returns pathTrie
func newPathTrie() *pathTrie {
	return &pathTrie{
		root:  newNode(),
		bpool: bpool.NewBufferPool(48),
	}
}

// wilKey is constant to access wild child in trie
const wildKey string = "*"
const routerKey string = "router"

// add an given http.Handler to method and path
func (trie *pathTrie) add(method string, path string, handler http.Handler) error {
	// check if handler is not nil
	if handler == nil {
		return ErrHandler
	}

	// parse path
	keys, err := parsePath(path)

	// check if parsing has been successful
	if err != nil {
		defer log.Fatal(err)
		return err
	}

	// set current's cursor position to trie root
	cursor := trie.root

	// iterate and build trie
	for _, key := range keys {
		// check if entry is sub-router
		if cursor.isRouter {
			return err
		}

		// parse key
		properties := parseKey(key)

		// check whether key is wildcard or named key/path
		if properties.isNamedKey || properties.isNamedPath {
			if len(cursor.children) != 0 {
				return err
			}

			// child is named key / named path
			cursor.isWildChild = true

			// create new node
			cursor.children[wildKey] = newNode()
			cursor.children[wildKey].setNodeProperties(properties)

			cursor = cursor.children[wildKey]
		} else if cursor.children[key] == nil {
			// create new node
			cursor.children[key] = newNode()
			cursor.children[key].setNodeProperties(properties)

			cursor = cursor.children[key]
		} else {
			cursor = cursor.children[key]
		}
	}

	// create handler map
	if cursor.handler == nil {
		cursor.handler = make(map[string]http.Handler)
	}

	// assign handler
	if cursor.handler[method] == nil && !cursor.isRouter {
		cursor.handler[method] = handler
	} else {
		defer log.Fatal(ErrPathAssignment)
		return ErrPathAssignment
	}

	// no errors
	return nil
}

// addRouter adds a router for sub-routing to given path
func (trie *pathTrie) addRouter(path string, handler http.Handler) error {
	// check if handler is not nil
	if handler == nil {
		return ErrHandler
	}

	// parse path
	keys, err := parsePath(path)

	// check if parsing has been successful
	if err != nil {
		return err
	}

	// set current's cursor position to trie root
	cursor := trie.root

	// iterate and build trie
	for _, key := range keys {
		// parse key
		properties := parseKey(key)

		// no named paths allowed here
		if properties.isNamedPath {
			return ErrPathAssignment
		}

		// create node
		if cursor.children[key] == nil {
			cursor.children[key] = newNode()
			cursor.children[key].setNodeProperties(properties)

			cursor = cursor.children[key]
		} else {
			cursor = cursor.children[key]
		}
	}

	if cursor.handler != nil || len(cursor.children) > 0 {
		return ErrPathAssignment
	}

	// assign handler
	cursor.handler = make(map[string]http.Handler)
	cursor.handler[routerKey] = handler
	cursor.isRouter = true

	// no errors
	return nil
}

// get is looking for the specified entry in its pathTrie
// It returns the associated http.Handler or nil
func (trie pathTrie) get(method string, path string) (http.Handler, map[string]string, error) {
	// parse path
	keys, err := parsePath(path)

	// check for parsing errors
	if err != nil {
		return nil, nil, err
	}

	// params map
	var params map[string]string

	// set current's cursor position to trie root
	cursor := trie.root

	// iterate over trie
	for i, key := range keys {
		// access current node's children
		if cursor.isWildChild {
			// create empty params map
			if params == nil {
				params = make(map[string]string)
			}

			// access wild child
			cursor = cursor.children[wildKey]

			// Check if current node is named path
			if cursor.isNamedPath {
				// get buffer from pool
				buffer := trie.bpool.Get()

				// reconstruct content for named path
				for _, elem := range keys[i:] {
					buffer.WriteString("/")
					buffer.WriteString(elem)
				}

				// assign named path value to params map
				params[cursor.key] = buffer.String()

				// put back buffer to pool
				trie.bpool.Put(buffer)

				// return handler and params map
				return cursor.handler[method], params, nil
			}

			// Check if current node is named key
			if cursor.isNamedKey {
				params[cursor.key] = key
			}
		} else {
			// return sub-router if current child is router
			if cursor.isRouter {
				return cursor.handler[routerKey], params, nil
			}

			// access next child
			cursor = cursor.children[key]

			if cursor == nil {
				// return nil, no next child
				return nil, nil, nil
			}
		}
	}

	// return handler
	return cursor.handler[method], params, nil
}
